# Mutt Match

Find a dog from Manhattan's ACC based on a the personality insights of a user's tweets and the dog's description using Ibm Watson Personality Insights API.   

## Run the app locally

1. [Install Node.js][]
2. cd into the app directory
3. Run `npm install` to install the app's dependencies
4. Run `npm start` to start the app
5. Access the running app in a browser at http://localhost:6001

[Install Node.js]: https://nodejs.org/en/download/
