var express = require('express');
var router = express.Router();


var async = require('async');
var bctwitter = require('../lib/tw.js')
var bcwatson = require('../lib/watson.js')
var similar = require('../lib/similarity.js')
var d = require("../data/dogs.js")


var userTwtTxt = "";
var userProfile = "";

router.get('/', function(req, res, next) {
    res.render('index', { title: 'Mutt Match!'});
});


router.post('/match', function (req, res, next) {
    var username = req.body.username

    async.series([
        function (callback) {
            bctwitter.getUserTweetText(username, function (err, twTxt) {
                if (err) return callback(err);
                userTwtTxt = twTxt;
                callback();
            });
        },
        function (callback) {
            bcwatson.getUserProfile(userTwtTxt, function (err, profile) {
                if (err) return callback(err);
                userProfile = profile;
                callback();
            });
        }
    ], function (err) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) return next(err); //If an error occurred, we let express handle it by calling the `next` function
        var simpleUserProfile = bcwatson.getSimpleProfile(userProfile);
        var match = {};

        var allDogs = [];
        for (var key in d.dogs) {
            var s = similar.similaritySimple(simpleUserProfile, d.dogs[key].simpleProfile);
            allDogs.push({dog: d.dogs[key].name, score: s});
            if (match.score == null) {
                match.matcheddog = {};
                match.matcheddog = d.dogs[key];


                match.score = s;
            }
            else {
                if (match.score <= s) {
                    match.matcheddog = d.dogs[key];
                    match.score = s;
                }
            }
        }

        match.alldogs = allDogs;
        match.username = username;
        match.userprofile = simpleUserProfile;
        console.log(JSON.stringify(match))
        res.render('match', {
            result: match
        })
    });
});

module.exports = router;

