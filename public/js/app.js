$(document).ready(function () {

    // process the form
    $('form').submit(function (event) {
        var formData = {
            'username': $('input[name=username]').val()
        };

        $.ajax({
            type: 'POST', 
            url: '/match', 
            data: formData, 
            dataType: 'text', 
            encode: true
        }).done(function (data) {
                console.log("DONE:")
                console.log(data)
                $('#matchcontent').html(data)
        }).fail(function (err) {
            console.log("ERROR: ")
            console.log(err);
            $('#matchcontent').text("Ooops, something went wrong")
        });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});