var dogs =

    [{
            "name": "BOLO",
            "id": "A0981987",
            "link": "http://www.petharbor.com/pet.asp?uaid=NWYK1.A0981987",
            "desc": "Bolo is a mature gentleman--8 years old. Mature dogs are great to adopt because they are past those difficult puppy years but have lots of good mileage on them. Come meet this handsome guy. He sports a midnight black coat with a snow white vest. Quite a looker is he! Bolo can become your new best friend but you have to reach out to him. He is patiently awaiting your arrival. Don't disappoint Bolo. Bolo shows all the telltale signs of being a Boxer mix-- his tail never stops wagging, he's very eager to please, and you look away for one second only to find him rolling over for a belly rub. He also has the characteristic folded-over ears, white paws and chest, and white diamond on his back. Bolo is very friendly and energetic, loves to give kisses (and licks!), and has a strong sense of smell (in fact, I only realized I had forgotten to zip up my fanny pack when Bolo leaned in to slyly pull out a milk bone!). It's hard to believe Bolo is really 8 years old-- his energy, friendliness, and intelligence make him seem like a younger man, but his salt-and-pepper hair gives him away.",
            "pic": "http://www.petharbor.com/get_image.asp?RES=Detail&ID=A0981987&LOCATION=NWYK1",
            "simpleProfile": [{
                "name": "Openness",
                "percentage": 0.9719973835711064
            }, {
                "name": "Conscientiousness",
                "percentage": 0.09253974363553047
            }, {
                "name": "Extraversion",
                "percentage": 0.08705885976637562
            }, {
                "name": "Agreeableness",
                "percentage": 0.22644836957589248
            }, {
                "name": "Emotional range",
                "percentage": 0.17330500823438627
            }]
        }, {
            "name": "TAMMIE",
            "id": "A1072755",
            "link": "http://www.petharbor.com/pet.asp?uaid=NWYK.A1072755",
            "desc": "Tammie brings out the child in me. She may be 3 years-old but she's as playful and spry as a pup half her age. I affectionately deemed her 'my little seal pup' not long after our first meeting. She is adorably round, with her stout, little legs, and her gorgeous face often seems too small for her gorgeous body. She bounds about our pen, batting away toys, and returning to my side each time. Tammie may be childlike in some ways but she is also a sophisticated, elegant lady who knows what she wants and when she wants it. When I take too long to produce a treat, 'WOOF!' (Translation: 'Now, please!') When I step away from her momentarily to speak to a fellow volunteer, 'WOOF!' (Translation: 'Hello! Pay attention to ME, please!') But do not be fooled. Tammie is not ill-behaved, not in the slightest. She is sensitive, curious, bright, and communicative. She's merely telling you what she wants, which is the key to any successful relationship! The moment I produce a treat and return to her side, she flashes that megawatt smile and falls quiet once more. Tammy is a terrific (and humorous) walker (waddler?) on leash, appears to be housebroken, and thoroughly enjoys her treats. She will sit for them if you ask her (nicely). Just don't leave her waiting too long. Tammie is waiting for you (and your treats!) at Manhattan's ACC. I'm a 3 year old friendly doggie with a gentle, sweet energy. I love people; especially ones that give me massages, treats, and play fetch with me. While I am more on the mellow side, I can be peppy too! I love to play, even by myself. If you give me a stick (or chew toy), I will be entertained for hours! I walk really well on a leash and love going for long walks at the park. At the end of the day, I love nothing more than to snuggle up next to you, with a nice cozy blanket. I have a lot of love to give, so please help me find my forever family! Tammie is a legend already--much loved by staff and volunteers. Super friendly, great in play group, she's friendly and outgoing. Tammie would absolutely be voted Most Popular and Most Likely to Succeed were we all in high school. ",
            "pic": "http://www.petharbor.com/get_image.asp?RES=Detail&ID=A1072755&LOCATION=NWYK",
            "simpleProfile": [{
                "name": "Openness",
                "percentage": 0.18820443580359128
            }, {
                "name": "Conscientiousness",
                "percentage": 0.7008537746873774
            }, {
                "name": "Extraversion",
                "percentage": 0.9734966461314
            }, {
                "name": "Agreeableness",
                "percentage": 0.99
            }, {
                "name": "Emotional range",
                "percentage": 0.45138333177384354
            }]
        }, {
            "name": "JIGGY",
            "id": "A1085999",
            "link": "http://www.petharbor.com/pet.asp?uaid=NWYK.A1085999",
            "desc": "When another volunteer tells me how awesome a dog is, that dog is moved right to the top of my 'must meet' list. And Jiggy didn't disappoint! Surrendered to our care due to landlord issues, Jiggy comes with an outstanding resume! He enjoys playing with children (although can be a little rough in play not realizing he's big and they're little), he's friendly with everyone, is housetrained, doesn't guard his things and may even think you're playing if you take a toy away. I loved my time with Jiggy - he showed off his housetraining skills the moment we were out the door, and even hopped into the street to do his business (not on the sidewalk). He loves to cuddle crawling onto my lap, yes all of him, to snuggle and give me kisses. If I stopped petting him he'd nudge me with his nose to 'carry on'. Wagging his stubby little adorable tail at every dog we pass, on his first play date he engaged in bouncy play with the female helper. His fabulous personality is equaled by his stunning good looks and grooming. Jiggy is clearly a dog who was a beloved family companion and he's ready to find true love again. Don't make him wait long, come meet him today. Can you feel the love? Jiggy is one of those rare pups who not only grabs your heart but is also a true bundle of fun! Show him a toy and see his eyes light up, take him for a walk and he will gaze up adoringly, making sure everything is 'ok.' But the best is when he nuzzles up to lie gently in your lap, his heart beating softly beneath the brilliant white of his coat; his trust is instant and implicit. Beautifully cared for by his former family, Jimmy's notes from them are filled with wonderful comments such as he enjoys playing exuberantly with children and is well-mannered around other dogs. Being a youngster, Jiggy might need some guidance to curtail some of his 'puppiness' when at play, but no doubt he's eager to learn. So, lace up your sneakers and come meet Jiggy. You'll find yourself a loving forever friend!",
            "pic": "http://www.petharbor.com/get_image.asp?RES=Detail&ID=A1085999&LOCATION=NWYK",
            "simpleProfile": [{
                "name": "Openness",
                "percentage": 0.9940603101460703
            }, {
                "name": "Conscientiousness",
                "percentage": 0.07239046798894044
            }, {
                "name": "Extraversion",
                "percentage": 0.5340516824552183
            }, {
                "name": "Agreeableness",
                "percentage": 0.7154867384932144
            }, {
                "name": "Emotional range",
                "percentage": 0.20279124469430365
            }]
        }, {
            "name": "BONAPARTE",
            "id": "A1087136",
            "link": "http://www.petharbor.com/pet.asp?uaid=NWYK.A1087136",
            "desc": "Hi everyone - I'm Bonaparte! Things are looking better these days after I was left in an empty apartment when my owner was evicted. I'm underweight, but I'm getting three meals a day here and I'm soooooo happy about that. I love treats too, and my new friend named Volunteer let me steal all the treats she had because she knew I was hungry. I know I need to go slow as too much food too fast isn't good for me, although I'm still pretty hungry much of the time. I have some sores on my hips since there's no padding there, but as soon as I'm at a good weight and taken care of properly those will go away. My second favorite thing (food being my first right now) is snuggling. Offer me a lap and a hug and I'm right there! It feels soooo good to be loved like that. I like to pose for pictures too, as it makes me feel good that someone thinks I'm handsome enough to photograph. Volunteer kept telling me I need to pose nicely as good pictures will get me 'adopted'. I think that means that they will help me find a good home and I'm all for that! I'm curious about stuff and like to explore, and Volunteer and another friend named Staff laughed at my pink nose that I stuck into the camera lens. Oh well..... The people here are so nice and think I'll do well in a home with average dog experience. I think so too as long as there are laps to snuggle in and lots of good nutritious food. So, come meet me as to know me is to love me and I'm pretty sure you'll want to take me home. At least, I hope so.",
            "pic": "http://www.petharbor.com/get_image.asp?RES=Detail&ID=A1087136&LOCATION=NWYK",
            "simpleProfile": [{
                "name": "Openness",
                "percentage": 0.5766772329375064
            }, {
                "name": "Conscientiousness",
                "percentage": 0.4180858426905809
            }, {
                "name": "Extraversion",
                "percentage": 0.7165323822261052
            }, {
                "name": "Agreeableness",
                "percentage": 0.9951138445199299
            }, {
                "name": "Emotional range",
                "percentage": 0.02925097057041459
            }]
        }, {
            "name": "ROCKY",
            "id": "A1079076",
            "link": "http://www.petharbor.com/pet.asp?uaid=NWYK1.A1079076",
             "desc": "The little bit of time it takes Rocky to warm up is a small price to pay for his company...he is so, so sweet. So sweet that I keep expecting him to caramelize during this summer heat wave! He was with his family since he was a puppy, so it's no surprise that he's can be a little nervous and shy. He is quiet as a mouse in his cage, walks perfectly on leash, and seems very housebroken. We're told he has a high activity level, doesn't have any guarding issues, loves to play with balls, and followed his people around the house. But he did become uncomfortable with the child in the home, and also when things got loud during a sports broadcast, so he might prefer a calm, quiet environment. He lived with a small dog and 'they loved each other' -- I don't even need to know the other dog to say this was probably the cutest duo in the five boroughs! When I first met Rocky, he was unsure, but seemed comforted by petting, but after spending more time with him he has proven to be very affectionate. He is waggy when I approach his cage, leans all of his weight against me, sometimes does an adorable lying-under-the-bench-placing-his-head-between-my-feet move, and will prop his paws on my lap and solicit endless cuddles. He is so well-mannered and very much wants to connect with people -- what a gentle, sensitive, sweet little man! Please come give him a chance at Brooklyn ACC.",        
            "pic": "http://www.petharbor.com/get_image.asp?RES=Detail&ID=A1079076&LOCATION=NWYK1",
            "simpleProfile": [{
                "name": "Openness",
                "percentage": 0.9593531209612417
            }, {
                "name": "Conscientiousness",
                "percentage": 0.04055032267093267
            }, {
                "name": "Extraversion",
                "percentage": 0.17063418565255928
            }, {
                "name": "Agreeableness",
                "percentage": 0.4393269725761298
            }, {
                "name": "Emotional range",
                "percentage": 0.20245619793762643
            }]
        }

    ]



module.exports.dogs = dogs;
