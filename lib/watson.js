var watson = require('watson-developer-cloud');

var personality_insights = watson.personality_insights({
    username: 'f2ee40db-be3f-416b-9f70-5b0337b0dc71',
    password: 'pAQ1nXzXCeXG',
    version: 'v2'
});

function getUserProfile(text, callback) {

    personality_insights.profile({text: text}, function (error, userPersonality) {
        if (error) {
            callback(error,"");
        }
        else{
            callback("", userPersonality);
        }
    });

}

function getSimpleProfile(profile) {
    profile = typeof (profile) === 'string' ? JSON.parse(profile) : profile;
    var big5 = profile.tree.children[0].children[0].children;
    var simpleProfile = [];
    big5.forEach(function (trait, i) {
        tr = { name: trait.name, percentage: trait.percentage}
        //console.log("trait:" + trait.name + " % " + trait.percentage);
        simpleProfile.push(tr)
    });
    return simpleProfile;

}


module.exports.getUserProfile = getUserProfile;
module.exports.getSimpleProfile = getSimpleProfile;